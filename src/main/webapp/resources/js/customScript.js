$(document).ready(function() {

    $('li.menu').removeClass("active");
    if(document.location.pathname.indexOf("matches") > -1) {
        $("a:contains('Planned Matches')").parent("li").addClass("active");
    } else if(document.location.pathname.indexOf("goalscorers") > -1) {
        $("a:contains('Goals Scorers')").parent("li").addClass("active");
    } else if(document.location.pathname.indexOf("cards") > -1) {
        $("a:contains('Cards')").parent("li").addClass("active");
    } else if(document.location.pathname.indexOf("referees") > -1) {
        $("a:contains('Referees')").parent("li").addClass("active");
    } else {
        $("a:contains('Classification')").parent("li").addClass("active");
    }

    $.tablesorter.addParser({ 
        id: 'goals', 
        is: function(s) { return false; }, 
        format: function(s) { return s.split(' ')[0]; }, 
        type: 'numeric' 
    }); 
     
    $("#classificationTable").tablesorter({ 
        headers: { 5: {sorter:'goals'}} 
    }); 
    
    $(document).ready(function() {
        var table = $('#classificationTable').DataTable({
            scrollY: $(document).height() - 380,
            "dom": 'rt'
        });
        
    $(window).bind('resize', function () {
    	  $(".dataTables_scrollBody").height($(document).height() - 380);
    	  myTable.fnDraw();
    	});

        new $.fn.dataTable.FixedHeader( table );
    } );
    
	$("#cardsClassificationTable").tablesorter();
    $("#goalClassificationTable").tablesorter();
});