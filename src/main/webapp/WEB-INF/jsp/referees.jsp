<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<t:genericGrid>
	<h1>Referees</h1>
	<table class="table table-striped">
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Surname</th>
			<th>Number of matches</th>
		</tr>
		<tbody>
			<c:forEach var="referee" items="${referees}">
				<tr>
					<td>${referee.id}</td>
					<td>${referee.name}</td>
					<td>${referee.surname}</td>
					<td>${fn:length(referee.refereedMatches)}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</t:genericGrid>