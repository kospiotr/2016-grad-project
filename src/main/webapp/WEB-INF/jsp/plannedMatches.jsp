<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:genericGrid>
    <h1>Planned matches ~~!</h1>
    	<c:forEach var="matchDay" items="${matchDays}">


			<h2># ${matchDay.id} - ${matchDay.date}</h2>
    		<table class="table table-striped">
    			<thead>
					<tr>
						<th>Home</th>
						<th>result</th>
						<th>Away</th>
						<th>Stadium</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="match" items="${matchDay.getList()}">
						<tr>
							<td>${match.home}</td>
							<td><a href="/competition/${competitionId}/season/${seasonId}/matches/${match.id}">${match.homeScore} : ${match.awayScore}</a></td>
							<td>${match.away}</td>
							<td>${match.stadium}</td>
						</tr>
					</c:forEach>
				</tbody>
    		</table>
    	</c:forEach>
</t:genericGrid>