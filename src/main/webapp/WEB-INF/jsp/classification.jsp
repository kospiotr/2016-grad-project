<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:genericGrid>
	<div class="dropdown" style="align:right;float:right;">
		<button class="btn btn-default btn-lg dropdown-toggle" type="button"
			data-toggle="dropdown">
			Classification <span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li><a href="?classification=home">Home</a></li>
			<li><a href="?classification=away">Away</a></li>
			<li><a href="?classification=total">Total</a></li>
		</ul>
	</div>
	<div class="container">
		<h1>${head}</h1><br />
		<table id="classificationTable"
			class="table table-responsive  table-hover table tablesorter">
			<thead>
				<tr>
					<th>#</th>
					<th>Team</th>
					<th>W</th>
					<th>D</th>
					<th>L</th>
					<th>Goals</th>
					<th>Points</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="team" items="${teams}">
					<tr>
						<td>${team.position}</td>
						<td>${team.teamName}</td>
						<td>${team.wonMatchesCount}</td>
						<td>${team.drewMatchesCount}</td>
						<td>${team.lostMatchesCount}</td>
						<td>${team.scoredGoals} : ${team.lostGoals}</td>
						<td>${team.points}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</t:genericGrid>
