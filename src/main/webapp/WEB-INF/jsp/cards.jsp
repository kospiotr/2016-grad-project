<%--
  Created by IntelliJ IDEA.
  User: rjan
  Date: 2015-09-04
  Time: 12:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:genericGrid>
    <h1>Cards</h1>
    <table id="cardsClassificationTable" class="table table-striped tablesorter">
        <thead>
        <th>Player</th>
        <th>Yellow Cards</th>
        <th>Red Cards</th>
        </thead>
        <tbody>
        <c:forEach items="${cards}" var="card">
            <tr>
                <td>${card.player}</td>
                <td>${card.yellowCardsCount}</td>
                <td>${card.redCardsCount}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</t:genericGrid>