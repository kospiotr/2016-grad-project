<%--
  Created by IntelliJ IDEA.
  User: rjan
  Date: 2015-09-03
  Time: 15:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:startedPageGrid>
  <c:forEach items="${competition.seasons}" var="season">
    <a class="btn btn-primary btn-block" href="/competition/${competition.id}/season/${season.id}">${season.year}</a>
  </c:forEach>
</t:startedPageGrid>