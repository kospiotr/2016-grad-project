<%--
  Created by IntelliJ IDEA.
  User: mwachowicz
  Date: 2015-09-03
  Time: 16:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:genericGrid>
  <h1> Top Scorers Goals</h1>
  <table id="goalClassificationTable" class="table table-striped tablesorter">
    <thead>
      <th>Player</th>
      <th>Goals</th>
      <th>Own Goals</th>
      <th>Team</th>
    </thead>
      <c:forEach var="goal" items="${goals}">
        <tr>
            <td>${goal.player}</td>
            <td>${goal.goalCount}</td>
            <td>${goal.ownGoalCount}</td>
            <td>${goal.team}</td>
        </tr>
      </c:forEach>

  </table>
</t:genericGrid>