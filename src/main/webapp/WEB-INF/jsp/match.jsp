<%--
  Created by IntelliJ IDEA.
  User: rjan
  Date: 2015-09-07
  Time: 10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:startedPageGrid>
    <div class="row">
        <div class="col-md-3 col-md-offset-1" style="text-align: center;">
            <h2>${match.home}</h2>
        </div>
        <div class="col-md-2 col-md-offset-1" style="text-align: center;">
            <h2>${match.homeScore}:${match.awayScore}</h2>
        </div>
        <div class="col-md-3 col-md-offset-1" style="text-align: center;">
            <h2>${match.away}</h2>
        </div>
        <div class="col-md-12" style="text-align: center;">
            ${match.stadium}
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        <a class="btn btn-success btn-block" href="edit/${match.id}">Edit</a>
        <a class="btn btn-warning btn-block" href="delete/${match.id}">Delete</a>
    </div>
</t:startedPageGrid>
