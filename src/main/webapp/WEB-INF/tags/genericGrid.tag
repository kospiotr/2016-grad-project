<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GFT Junior</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <!-- custom css -->
    <link href="/resources/css/custom.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
     <!-- Include data table plugin for scrolling -->
    <script type="text/javascript" src="https://cdn.datatables.net/r/bs/dt-1.10.9/datatables.min.js"></script>
    <!-- DataTables CSS -->
 	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.css">
    <!-- Table sorter -->
    <script src="/resources/js/jquery.tablesorter.js"></script>
    <!-- custom js-->
    <script type='text/javascript' src='/resources/js/customScript.js'></script>
</head>
<body>
<div id="header">
    <nav class="navbar navbar-inverse back_ground_img navbar-fixed-top navbar-custom ">
        <div class="container">
            <div class="collapse navbar-collapse" id="centerednav ">
				<ul class="nav navbar-nav">
                    <li class="menu active"><a href="/competition/${competitionId}/season/${seasonId}/" class="whiteMenuLink">Classification</a></li>
                    <li class="menu"><a href="/competition/${competitionId}/season/${seasonId}/matches" style="width: 320px; margin-left: -30px;" class="whiteMenuLink">Planned Matches</a></li>
                    <li class="menu"><a href="/competition/${competitionId}/season/${seasonId}/goalscorers" class="whiteMenuLink">Goals Scorers</a></li>
                    <li class="menu"><a href="/competition/${competitionId}/season/${seasonId}/cards" class="whiteMenuLink">Cards</a></li>
                    <li class="menu"><a href="/competition/${competitionId}/season/${seasonId}/referees" class="whiteMenuLink">Referees</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>


<!-- cotent body div -->
<div id="content-body">

    <!-- jsp cotent -->

    <jsp:doBody/>

</div>


<div class="footer"/>
<div class="footer-text">
    <span>Copyright (c) Supper Gradki</span>

    <p>PM: Piotr Kosmowski

    <p>
</div>
</div>
</div>
</body>
</html>