<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GFT Junior</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <!-- custom css -->
    <link href="/resources/css/custom.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <!-- custom js-->
    <script type='text/javascript' src='/resources/js/customScript.js'></script>


</head>
<body>
<div id="header">
    <nav class="navbar navbar-inverse back_ground_img navbar-fixed-top navbar-custom ">
        <div class="container">
            <div class="navbar-header">
                <!--<a class="navbar-brand" href="#">GFT Junior Training Programme</a>-->
                <span class="navbar-brand">GFT Junior Training Programme</span>
            </div>
        </div>
    </nav>
</div>


<!-- cotent body div -->
<div id="content-body">

    <!-- jsp cotent -->

    <jsp:doBody/>

</div>


<div class="footer"/>
<div class="footer-text">
    <span>Copyright (c) Supper Gradki</span>

    <p>PM: Piotr Kosmowski

    <p>
</div>
</div>
</div>
</body>
</html>