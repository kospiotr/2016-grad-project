package com.project.footballscores.controller;

import com.project.footballscores.model.Competition;
import com.project.footballscores.service.CompetitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by rjan on 2015-09-01.
 */
@Controller
public class WelcomeController {

    @Autowired
    CompetitionService competitionService;

    @RequestMapping("/")
    public String index(Model model) {
        List<Competition> competitions = competitionService.findAll();
        model.addAttribute("competitions", competitions);
        return "index";
    }

    @RequestMapping("/competition/{competitionId}")
    public String seasons(Model model, @PathVariable Long competitionId) {
        Competition competition = competitionService.findById(competitionId);
        model.addAttribute("competition", competition);
        return "seasons";
    }

    @RequestMapping("/grad")
    public ModelAndView table() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("table");
        return modelAndView;
    }
}
