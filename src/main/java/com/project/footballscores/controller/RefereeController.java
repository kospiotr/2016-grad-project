package com.project.footballscores.controller;

import com.project.footballscores.model.Competition;
import com.project.footballscores.model.Season;
import com.project.footballscores.service.CompetitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.project.footballscores.service.RefereeService;

@Controller
public class RefereeController {

	@Autowired
	private RefereeService refereeService;

	@Autowired
	CompetitionService competitionService;
	
	@RequestMapping("/competition/{competitionId}/season/{seasonId}/referees")
	public ModelAndView displayRefereePage(@PathVariable Long competitionId, @PathVariable Long seasonId) {
		ModelAndView modelAndView = new ModelAndView();
		Competition competition = competitionService.findById(competitionId);
		modelAndView.addObject("competition", competition);
		for(Season season : competition.getSeasons()) {
			if(season.getId() == seasonId) {
				modelAndView.addObject("season", season);
			}
		}
		modelAndView.setViewName("referees");
		modelAndView.addObject("referees", refereeService.findAllRefereesSortedByMatches());
		return modelAndView;
	}
}
