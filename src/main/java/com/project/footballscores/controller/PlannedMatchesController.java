package com.project.footballscores.controller;

import com.project.footballscores.service.CompetitionService;
import com.project.footballscores.service.PlannedMatchesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by isiejkowski on 2015-09-04.
 */

@Controller
public class PlannedMatchesController {

    @Autowired
    PlannedMatchesService plannedMatchesService;

    @RequestMapping("/competition/{competitionId}/season/{seasonId}/matches")
    public ModelAndView plannedMatches(@PathVariable(value="competitionId") final Long competitionId,
                                       @PathVariable(value="seasonId") final Long seasonId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("plannedMatches");
        modelAndView.addObject("matchDays", plannedMatchesService.getMatches(competitionId, seasonId));
        return modelAndView;
    }

}
