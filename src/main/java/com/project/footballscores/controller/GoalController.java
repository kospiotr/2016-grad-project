package com.project.footballscores.controller;

import com.project.footballscores.model.Competition;
import com.project.footballscores.model.Season;
import com.project.footballscores.service.CompetitionService;
import com.project.footballscores.service.GoalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by mwachowicz on 2015-09-03.
 */
@Controller
public class GoalController {
	@Autowired
	GoalService goalService;

	@Autowired
	CompetitionService competitionService;

	@RequestMapping("/competition/{competitionId}/season/{seasonId}/goalscorers")
	public String displayGoalsPage(Model model, @PathVariable Long seasonId, @PathVariable Long competitionId) {
		model.addAttribute("goals", goalService.getGoalsClassificationForSeason(seasonId));
		Competition competition = competitionService.findById(competitionId);
		model.addAttribute("competition", competition);
		for(Season season : competition.getSeasons()) {
			if(season.getId() == seasonId) {
				model.addAttribute("season", season);
			}
		}
		return "goal";
	}
}
