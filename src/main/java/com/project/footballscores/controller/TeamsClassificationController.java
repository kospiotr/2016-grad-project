package com.project.footballscores.controller;

import com.project.footballscores.service.TeamsClassificationService;
import com.project.footballscores.service.TeamsClassificationService.ClassificationType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TeamsClassificationController {

	@Autowired
	private TeamsClassificationService teamClassificationService;
	
    @RequestMapping("/competition/{competitionId}/season/{seasonId}")
    public ModelAndView displayClassificationPage(@PathVariable(value="competitionId") final Long competitionId,
    											  @PathVariable(value="seasonId") final Long seasonId,
    											  @RequestParam(value="classification", required = false) String classification) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("classification");
        
        if(classification == null || classification.equals("total")) {
        	modelAndView.addObject("teams", teamClassificationService.getTeamsClassification(competitionId, seasonId, ClassificationType.ALL_MATCHES));
        	modelAndView.addObject("head", teamClassificationService.getClassificationHeader(competitionId, seasonId, ClassificationType.ALL_MATCHES));
        } else if(classification.equals("home")) {
    		modelAndView.addObject("teams", teamClassificationService.getTeamsClassification(competitionId, seasonId, ClassificationType.HOME_MACTHES));
    		modelAndView.addObject("head", teamClassificationService.getClassificationHeader(competitionId, seasonId, ClassificationType.HOME_MACTHES));
        } else {
        	modelAndView.addObject("teams", teamClassificationService.getTeamsClassification(competitionId, seasonId, ClassificationType.AWAY_MATCHES));
        	modelAndView.addObject("head", teamClassificationService.getClassificationHeader(competitionId, seasonId, ClassificationType.AWAY_MATCHES));
        }
        return modelAndView;
    }
}
