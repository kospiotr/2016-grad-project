package com.project.footballscores.controller;

import com.project.footballscores.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by rjan on 2015-09-07.
 */
@Controller
@RequestMapping("/competition/{competitionId}/season/{seasonId}/matches")
public class MatchController {

    @Autowired
    MatchService matchService;

    @RequestMapping("/{matchId}")
    public String displayMatch(Model model, @PathVariable Long matchId) {
        model.addAttribute("match", matchService.findById(matchId));
        return "match";
    }
}