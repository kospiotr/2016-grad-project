package com.project.footballscores.controller;

import com.project.footballscores.model.CardClassificationTableRow;
import com.project.footballscores.model.Competition;
import com.project.footballscores.model.Season;
import com.project.footballscores.service.CardService;
import com.project.footballscores.service.CompetitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

/**
 * Created by rjan on 2015-09-04.
 */
@Controller
@RequestMapping("/competition/{competitionId}/season/{seasonId}")
public class CardController {

    @Autowired
    CardService cardService;

    @Autowired
    CompetitionService competitionService;

    @RequestMapping("/cards")
    public String displayCardsClassification(Model model, @PathVariable Long seasonId, @PathVariable Long competitionId) {
        List<CardClassificationTableRow> cardClassificationTableRows = cardService.getCardsClassificationForSeason(seasonId);
        Competition competition = competitionService.findById(competitionId);
        model.addAttribute("competition", competition);
        for(Season season : competition.getSeasons()) {
            if(season.getId() == seasonId) {
                model.addAttribute("season", season);
            }
        }
        model.addAttribute("cards", cardClassificationTableRows);
        return "cards";
    }
}
