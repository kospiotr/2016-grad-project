package com.project.footballscores.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by isiejkowski on 2015-09-07.
 */
public class PlannedMatchesMatchDayRow {

    private Long id;
    private String date;
    private List<PlannedMatchesRow> list = new ArrayList<PlannedMatchesRow>();

    public PlannedMatchesMatchDayRow(Long id, String date, List<PlannedMatchesRow> list) {
        this.id = id;
        this.date = date;
        this.list = list;
    }

    public PlannedMatchesMatchDayRow() {
    }

    public void add(PlannedMatchesRow plannedMatchesRow){

        getList().add(plannedMatchesRow);
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<PlannedMatchesRow> getList() {
        return list;
    }

    public void setList(List<PlannedMatchesRow> list) {
        this.list = list;
    }
}
