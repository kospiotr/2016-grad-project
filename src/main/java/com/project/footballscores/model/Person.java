package com.project.footballscores.model;

import javax.persistence.*;

@MappedSuperclass
public abstract class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="name"  , length=30 , nullable=false , unique=false)
	private String name;
	@Column(name="surname"  , length=30 , nullable=false , unique=false)
	private String surname;
	@Column(name="country"  , length=3 , nullable=false , unique=false)
	private String country;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
}
