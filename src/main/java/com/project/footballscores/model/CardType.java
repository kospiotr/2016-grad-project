package com.project.footballscores.model;

public enum CardType {
	YELLOW("Y"), RED("R");

	private final String cardType;

	CardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardType() {
		return cardType;
	}
}
