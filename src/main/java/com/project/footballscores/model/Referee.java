package com.project.footballscores.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "referees")
public class Referee extends Person {
	@OneToMany(mappedBy = "referee", fetch = FetchType.EAGER)
	private List<Match> refereedMatches = new ArrayList<>();

	public List<Match> getRefereedMatches() {
		return refereedMatches;
	}
	public void setRefereedMatches(List<Match> refereedMatches) {
		this.refereedMatches = refereedMatches;
	}
}
