package com.project.footballscores.model;

/**
 * Created by rjan on 2015-09-04.
 */
public class CardClassificationTableRow {
    private String player;
    private Short yellowCardsCount;
    private Short redCardsCount;

    public CardClassificationTableRow() {
    }

    public CardClassificationTableRow(String player, Short yellowCardsCount, Short redCardsCount) {
        this.player = player;
        this.yellowCardsCount = yellowCardsCount;
        this.redCardsCount = redCardsCount;
    }

    public String getPlayer() {
        return player;
    }
    public void setPlayer(String player) {
        this.player = player;
    }
    public Short getYellowCardsCount() {
        return yellowCardsCount;
    }
    public void setYellowCardsCount(Short yellowCardsCount) {
        this.yellowCardsCount = yellowCardsCount;
    }
    public Short getRedCardsCount() {
        return redCardsCount;
    }
    public void setRedCardsCount(Short redCardsCount) {
        this.redCardsCount = redCardsCount;
    }
}
