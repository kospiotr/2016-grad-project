package com.project.footballscores.model;

public enum PlayerPosition {
	GOALKEEPER("GK"), DEFENDER("DEF"), MIDFIELDER("MID"), ATTACKER("ST");

	private final String position;

	PlayerPosition(String position) {
		this.position = position;
	}

	public String getPosition() {
		return position;
	}
}
