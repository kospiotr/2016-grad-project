package com.project.footballscores.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "matchdays")
public class Matchday {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="date"   , nullable=false , unique=false)
	private Date date;
	@ManyToOne (optional=false)
	@JoinColumn(name="season_id", referencedColumnName = "id" , nullable=false , unique=false , insertable=true, updatable=true)
	private Season season;
	@OneToMany(mappedBy = "matchday", fetch = FetchType.EAGER)
	private List<Match> matches;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public List<Match> getMatches() {
		return matches;
	}
	public void setMatches(List<Match> matches) {
		this.matches = matches;
	}
	public Season getSeason() {
		return season;
	}
	public void setSeason(Season season) {
		this.season = season;
	}
}
