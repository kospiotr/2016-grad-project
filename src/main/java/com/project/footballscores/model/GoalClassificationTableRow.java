package com.project.footballscores.model;

/**
 * Created by mwachowicz on 2015-09-04.
 */
public class GoalClassificationTableRow {
    private String player;
    private int goalCount;
    private int ownGoalCount;
    private String team;

    public GoalClassificationTableRow(String player, int goalCount, int ownGoalCount, String team) {
        this.player = player;
        this.goalCount = goalCount;
        this.ownGoalCount = ownGoalCount;
        this.team = team;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public int getGoalCount() {
        return goalCount;
    }

    public void setGoalCount(int goalCount) {
        this.goalCount = goalCount;
    }

    public int getOwnGoalCount() {
        return ownGoalCount;
    }

    public void setOwnGoalCount(int ownGoalCount) {
        this.ownGoalCount = ownGoalCount;
    }

    public String getTeam() {return team;}

    public void setTeam(String team) {
        this.team = team;
    }
}
