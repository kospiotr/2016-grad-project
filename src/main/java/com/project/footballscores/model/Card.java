package com.project.footballscores.model;

import com.project.footballscores.converter.CardTypeConverter;

import javax.persistence.*;

@Entity
@Table(name = "cards")
public class Card {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="minute"   , nullable=false , unique=false)
	private Short minute;
	@ManyToOne(optional=false)
	@JoinColumn(name="match_id", referencedColumnName = "id" , nullable=false , unique=false , insertable=true, updatable=true)
	private Match match;
	@Column(name="type"  , length=3 , nullable=false , unique=false)
	@Convert(converter = CardTypeConverter.class)
	private CardType type;
	@ManyToOne (optional=false)
	@JoinColumn(name="player_id", referencedColumnName = "id" , nullable=false , unique=true  , insertable=true, updatable=true)
	private Player player;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Short getMinute() {
		return minute;
	}
	public void setMinute(Short minute) {
		this.minute = minute;
	}
	public Match getMatch() {
		return match;
	}
	public void setMatch(Match match) {
		this.match = match;
	}
	public CardType getType() {
		return type;
	}
	public void setType(CardType type) {
		this.type = type;
	}
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
}
