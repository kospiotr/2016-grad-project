package com.project.footballscores.model;

public enum GoalType {
	PENALTY("PEN"), OWN_GOAL("OWN"), NORMAL("NOR");

	private final String goalType;

	GoalType(String goalType) {
		this.goalType = goalType;
	}

	public String getGoalType() {
		return goalType;
	}
}
