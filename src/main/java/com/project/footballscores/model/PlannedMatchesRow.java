package com.project.footballscores.model;

import java.util.Date;

/**
 * Created by isiejkowski on 2015-09-04.
 */
public class PlannedMatchesRow {
    private Long id;
    private String home;
    private String away;
    private int homeScore;
    private int awayScore;
    private String stadium;


    public PlannedMatchesRow() {
    }

    public PlannedMatchesRow(Long id, String home, String away, int homeScore, int awayScore, String stadium) {
        this.id = id;
        this.home = home;
        this.away = away;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
        this.stadium = stadium;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getAway() {
        return away;
    }

    public void setAway(String away) {
        this.away = away;
    }

    public int getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(int homeScore) {
        this.homeScore = homeScore;
    }

    public int getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(int awayScore) {
        this.awayScore = awayScore;
    }

    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }
}
