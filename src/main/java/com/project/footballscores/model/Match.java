package com.project.footballscores.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "matches")
public class Match {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@OneToOne(optional=false)
	@JoinColumn(name="matchday_id", referencedColumnName = "id" , nullable=false , unique=false , insertable=true, updatable=true)
	private Matchday matchday;
	@OneToOne(optional=false)
	@JoinColumn(name="matchday_team_home_id", referencedColumnName = "id" , nullable=false , unique=false , insertable=true, updatable=true)
	private MatchdayTeam homeTeam;
	@OneToOne(optional=false)
	@JoinColumn(name="matchday_team_away_id", referencedColumnName = "id" , nullable=false , unique=false , insertable=true, updatable=true)
	private MatchdayTeam awayTeam;
	@OneToOne(optional=false)
	@JoinColumn(name="referee_id", referencedColumnName = "id" , nullable=false , unique=false , insertable=true, updatable=true)
	private Referee referee;
	@OneToOne(optional=false)
	@JoinColumn(name="stadium_id", referencedColumnName = "id" , nullable=false , unique=false , insertable=true, updatable=true)
	private Stadium stadium;
	@OneToMany(mappedBy = "match", fetch = FetchType.EAGER)
	private List<Goal> goals;
	@OneToMany(mappedBy = "match", fetch = FetchType.EAGER)
	private List<Card> cards;
	@OneToMany(mappedBy = "match", fetch = FetchType.EAGER)
	private List<Change> changes;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public MatchdayTeam getHomeTeam() {
		return homeTeam;
	}
	public void setHomeTeam(MatchdayTeam homeTeam) {
		this.homeTeam = homeTeam;
	}
	public MatchdayTeam getAwayTeam() {
		return awayTeam;
	}
	public void setAwayTeam(MatchdayTeam awayTeam) {
		this.awayTeam = awayTeam;
	}
	public Referee getReferee() {
		return referee;
	}
	public void setReferee(Referee referee) {
		this.referee = referee;
	}
	public Stadium getStadium() {
		return stadium;
	}
	public void setStadium(Stadium stadium) {
		this.stadium = stadium;
	}
	public List<Goal> getGoals() {
		return goals;
	}
	public void setGoals(List<Goal> goals) {
		this.goals = goals;
	}
	public List<Card> getCards() {
		return cards;
	}
	public void setCards(List<Card> cards) {
		this.cards = cards;
	}
	public List<Change> getChanges() {
		return changes;
	}
	public void setChanges(List<Change> changes) {
		this.changes = changes;
	}
	public Matchday getMatchday() {
		return matchday;
	}
	public void setMatchday(Matchday matchday) {
		this.matchday = matchday;
	}
}
