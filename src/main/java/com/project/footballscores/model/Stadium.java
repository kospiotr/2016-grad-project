package com.project.footballscores.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "stadiums")
public class Stadium {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="name", length=30 , nullable=false , unique=false)
	private String name;
	@Column(name="capacity", nullable=false , unique=false)
	private Integer capacity;
	@Column(name="city", length=20 , nullable=false , unique=false)
	private String city;
	@Column(name="country", length=3 , nullable=false , unique=false)
	private String country;
	@OneToMany(mappedBy = "stadium")
	private List<Match> matchesThatTookPlace = new ArrayList<>();
	@OneToMany(mappedBy = "stadium")
	private List<Team> teamsThatOwn = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getCapacity() {
		return capacity;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public List<Match> getMatchesThatTookPlace() {
		return matchesThatTookPlace;
	}
	public void setMatchesThatTookPlace(List<Match> matchesThatTookPlace) {
		this.matchesThatTookPlace = matchesThatTookPlace;
	}
	public List<Team> getTeamsThatOwn() {
		return teamsThatOwn;
	}
	public void setTeamsThatOwn(List<Team> teamsThatOwn) {
		this.teamsThatOwn = teamsThatOwn;
	}
}
