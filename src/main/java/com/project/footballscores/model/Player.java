package com.project.footballscores.model;

import com.project.footballscores.converter.PlayerPositionConverter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "players")
public class Player extends Person {
	@Column(name="position"  , length=3 , nullable=false , unique=false)
	@Convert(converter = PlayerPositionConverter.class)
	private PlayerPosition position;
	@ManyToOne(optional=false)
	@JoinColumn(name="team_id", referencedColumnName = "id" , nullable=false , unique=false , insertable=true, updatable=true)
	private Team team;
	@OneToMany(mappedBy = "player")
	private List<Goal> playerGoals = new ArrayList<>();
	@OneToMany(mappedBy = "player")
	private List<Card> playerCards = new ArrayList<>();

	public PlayerPosition getPosition() {
		return position;
	}
	public void setPosition(PlayerPosition position) {
		this.position = position;
	}
	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}
	public List<Goal> getPlayerGoals() {
		return playerGoals;
	}
	public void setPlayerGoals(List<Goal> playerGoals) {
		this.playerGoals = playerGoals;
	}
	public List<Card> getPlayerCards() {
		return playerCards;
	}
	public void setPlayerCards(List<Card> playerCards) {
		this.playerCards = playerCards;
	}
}
