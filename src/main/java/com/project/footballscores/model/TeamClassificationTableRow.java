package com.project.footballscores.model;

public class TeamClassificationTableRow {

	public int position;
	public String teamName;
	public int lostMatchesCount;
	public int drawnMatchesCount;
	public int wonMatchesCount;
	public int scoredGoals;
	public int lostGoals;
	public int points;
	
	public TeamClassificationTableRow(String teamName) {
		this.teamName = teamName;
	}
	
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public int getLostMatchesCount() {
		return lostMatchesCount;
	}
	public void setLostMatchesCount(int lostMatchesCount) {
		this.lostMatchesCount = lostMatchesCount;
	}
	public int getDrewMatchesCount() {
		return drawnMatchesCount;
	}
	public void setDrewMatchesCount(int drewMatchesCount) {
		this.drawnMatchesCount = drewMatchesCount;
	}
	public int getWonMatchesCount() {
		return wonMatchesCount;
	}
	public void setWonMatchesCount(int wonMatchesCount) {
		this.wonMatchesCount = wonMatchesCount;
	}
	public int getScoredGoals() {
		return scoredGoals;
	}
	public void setScoredGoals(int scoredGoals) {
		this.scoredGoals = scoredGoals;
	}
	public int getLostGoals() {
		return lostGoals;
	}
	public void setLostGoals(int lostGoals) {
		this.lostGoals = lostGoals;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
}
