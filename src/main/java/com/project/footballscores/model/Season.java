package com.project.footballscores.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "seasons")
public class Season {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="year"   , nullable=false , unique=false)
	private Short year;
	@ManyToOne (optional=false)
	@JoinColumn(name="competition_id", referencedColumnName = "id" , nullable=false , unique=false , insertable=true, updatable=true)
	private Competition competition;
	@OneToMany(mappedBy = "season", fetch = FetchType.EAGER)
	private List<Matchday> matchdays;
	@ManyToMany
	@JoinTable(
			name = "seasons_teams",
			joinColumns = {@JoinColumn(name = "season_id", referencedColumnName = "id")},
			inverseJoinColumns = {@JoinColumn(name = "team_id", referencedColumnName = "id")})
	private List<Team> teams;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Short getYear() {
		return year;
	}
	public void setYear(Short year) {
		this.year = year;
	}
	public List<Matchday> getMatchdays() {
		return matchdays;
	}
	public void setMatchdays(List<Matchday> matchdays) {
		this.matchdays = matchdays;
	}
	public List<Team> getTeams() {
		return teams;
	}
	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}
	public Competition getCompetition() {
		return competition;
	}
	public void setCompetition(Competition competition) {
		this.competition = competition;
	}
}
