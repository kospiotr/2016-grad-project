package com.project.footballscores.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "matchday_teams")
public class MatchdayTeam {
	@Id
	private Long id;
	@ManyToOne(optional=false)
	@JoinColumn(name="team_id", referencedColumnName = "id" , nullable=false , unique=false , insertable=true, updatable=true)
	private Team team;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "matchday_teams_players",
			joinColumns = {@JoinColumn(name = "matchday_team_id", referencedColumnName = "id")},
			inverseJoinColumns = {@JoinColumn(name = "player_id", referencedColumnName = "id")})
	private List<Player> players;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<Player> getPlayers() {
		return players;
	}
	public void setPlayers(List<Player> players) {
		this.players = players;
	}
	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}
}
