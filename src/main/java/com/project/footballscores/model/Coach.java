package com.project.footballscores.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "coaches")
public class Coach extends Person {

	@OneToOne(optional=false)
	@JoinColumn(name="team_id", referencedColumnName = "id" , nullable=false , unique=false , insertable=true, updatable=true)
	private Team team;

	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}
}
