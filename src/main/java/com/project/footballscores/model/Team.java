package com.project.footballscores.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "teams")
public class Team {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="name", length=30, nullable=false, unique=false)
	private String name;
	@ManyToOne(optional = false)
	@JoinColumn(name="stadium_id", referencedColumnName = "id" , nullable=false , unique=false , insertable=true, updatable=true)
	private Stadium stadium;
	@OneToMany(mappedBy = "team", fetch = FetchType.EAGER)
	private List<Player> players;
	@OneToOne(mappedBy = "team")
	@JoinColumn(name = "team_id")
	private Coach coach;
	@ManyToMany
	@JoinTable(
			name = "seasons_teams",
			joinColumns = {@JoinColumn(name = "team_id", referencedColumnName = "id")},
			inverseJoinColumns = {@JoinColumn(name = "season_id", referencedColumnName = "id")})
	private List<Season> season;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<Player> getPlayers() {
		return players;
	}
	public void setPlayers(List<Player> players) {
		this.players = players;
	}
	public Stadium getStadium() {
		return stadium;
	}
	public void setStadium(Stadium stadium) {
		this.stadium = stadium;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Coach getCoach() {
		return coach;
	}
	public void setCoach(Coach coach) {
		this.coach = coach;
	}
	public List<Season> getSeason() {
		return season;
	}
	public void setSeason(List<Season> season) {
		this.season = season;
	}
}
