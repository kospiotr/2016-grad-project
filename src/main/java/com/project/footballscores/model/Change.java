package com.project.footballscores.model;

import javax.persistence.*;

@Entity
@Table(name = "changes")
public class Change {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="minute"   , nullable=false , unique=false)
	private Short minute;
	@ManyToOne(optional=false)
	@JoinColumn(name="match_id", referencedColumnName = "id" , nullable=false , unique=false , insertable=true, updatable=true)
	private Match match;
	@ManyToOne (optional=false)
	@JoinColumn(name="player_in_id", referencedColumnName = "id" , nullable=false , unique=true  , insertable=true, updatable=true)
	private Player incommingPlayer;
	@ManyToOne (optional=false)
	@JoinColumn(name="player_off_id", referencedColumnName = "id" , nullable=false , unique=true  , insertable=true, updatable=true)
	private Player offcommingPlayer;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Short getMinute() {
		return minute;
	}
	public void setMinute(Short minute) {
		this.minute = minute;
	}
	public Match getMatch() {
		return match;
	}
	public void setMatch(Match match) {
		this.match = match;
	}
	public Player getIncommingPlayer() {
		return incommingPlayer;
	}
	public void setIncommingPlayer(Player incommingPlayer) {
		this.incommingPlayer = incommingPlayer;
	}
	public Player getOffcommingPlayer() {
		return offcommingPlayer;
	}
	public void setOffcommingPlayer(Player offcommingPlayer) {
		this.offcommingPlayer = offcommingPlayer;
	}
}
