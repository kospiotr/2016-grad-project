package com.project.footballscores.repository;

import com.project.footballscores.model.Stadium;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by rjan on 2015-09-03.
 */
public interface StadiumRepository extends JpaRepository<Stadium, Long> {
}
