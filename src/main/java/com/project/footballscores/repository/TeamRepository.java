package com.project.footballscores.repository;

import com.project.footballscores.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by rjan on 2015-09-03.
 */
public interface TeamRepository extends JpaRepository<Team, Long> {
}
