package com.project.footballscores.converter;

import com.project.footballscores.model.GoalType;

import javax.persistence.AttributeConverter;

/**
 * Created by rjan on 2015-09-03.
 */
public class GoalTypeConverter implements AttributeConverter<GoalType, String> {
    @Override
    public String convertToDatabaseColumn(GoalType goalType) {
        return goalType.getGoalType();
    }

    @Override
    public GoalType convertToEntityAttribute(String s) {
        for(GoalType goalType : GoalType.values()) {
            if (goalType.getGoalType().equals(s)) {
                return goalType;
            }
        }
        return GoalType.NORMAL;
    }
}
