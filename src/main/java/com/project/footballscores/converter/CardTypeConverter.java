package com.project.footballscores.converter;

import com.project.footballscores.model.CardType;

import javax.persistence.AttributeConverter;

/**
 * Created by rjan on 2015-09-03.
 */
public class CardTypeConverter implements AttributeConverter<CardType, String> {
    @Override
    public String convertToDatabaseColumn(CardType cardType) {
        return cardType.getCardType();
    }

    @Override
    public CardType convertToEntityAttribute(String s) {
        for (CardType cardType : CardType.values()) {
            if(cardType.getCardType().equals(s)) {
                return cardType;
            }
        }
        return CardType.YELLOW;
    }
}
