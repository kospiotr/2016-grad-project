package com.project.footballscores.converter;

import com.project.footballscores.model.PlayerPosition;

import javax.persistence.AttributeConverter;

/**
 * Created by rjan on 2015-09-03.
 */
public class PlayerPositionConverter implements AttributeConverter<PlayerPosition, String> {

    @Override
    public String convertToDatabaseColumn(PlayerPosition playerPosition) {
        return playerPosition.getPosition();
    }

    @Override
    public PlayerPosition convertToEntityAttribute(String s) {
        for(PlayerPosition playerPosition : PlayerPosition.values()) {
            if (playerPosition.getPosition().equals(s)) {
                return playerPosition;
            }
        }
        return PlayerPosition.GOALKEEPER;
    }
}
