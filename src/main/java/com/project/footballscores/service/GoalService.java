package com.project.footballscores.service;

import com.project.footballscores.model.*;
import com.project.footballscores.repository.GoalRepository;
import com.project.footballscores.repository.SeasonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by mwachowicz on 2015-09-02.
 */
@Service
public class GoalService {
    public GoalService(){}

    @Autowired
    SeasonRepository seasonRepository;

    public List<GoalClassificationTableRow> getGoalsClassificationForSeason(Long id){
        Map<String, GoalClassificationTableRow> goalClassificationTableRows = new HashMap<>();

        Season season = seasonRepository.findOne(id);
        for (Matchday matchday : season.getMatchdays()) {
            for (Match match : matchday.getMatches()) {
                for (Goal goal : match.getGoals()) {
                    String key = goal.getPlayer().getName() + " " + goal.getPlayer().getSurname();
                    int goalToAdd = 0;
                    int ownGoalToAdd = 0;
                    switch (goal.getType()) {
                        case OWN_GOAL:
                            ownGoalToAdd++;
                            break;
                        default:
                            goalToAdd++;
                            break;
                    }
                    if (goalClassificationTableRows.containsKey(key)) {
                        int goals = goalClassificationTableRows.get(key).getGoalCount();
                        int ownGoals = goalClassificationTableRows.get(key).getOwnGoalCount();
                        goalClassificationTableRows.get(key).setGoalCount( (goals + goalToAdd));
                        goalClassificationTableRows.get(key).setOwnGoalCount( (ownGoals + ownGoalToAdd));
                    } else {
                        goalClassificationTableRows.put(key, new GoalClassificationTableRow(key, goalToAdd, ownGoalToAdd, goal.getPlayer().getTeam().getName()));
                    }
                }
            }
        }
        List<GoalClassificationTableRow> goals = new ArrayList<>(goalClassificationTableRows.values());
        Collections.sort(goals, new Comparator<GoalClassificationTableRow>() {
            @Override
            public int compare(GoalClassificationTableRow o1, GoalClassificationTableRow o2) {
                return o2.getGoalCount() - o1.getGoalCount();
            }
        });
        return goals;

    }

}
