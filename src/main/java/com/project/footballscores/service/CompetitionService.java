package com.project.footballscores.service;

import com.project.footballscores.model.Competition;
import com.project.footballscores.repository.CompetitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rjan on 2015-09-03.
 */
@Service
public class CompetitionService {
    @Autowired
    CompetitionRepository competitionRepository;

    public List<Competition> findAll(){
        return competitionRepository.findAll();
    }

    public Competition findById(Long id) {
        return competitionRepository.findOne(id);
    }
}
