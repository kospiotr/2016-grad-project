package com.project.footballscores.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.footballscores.model.Competition;
import com.project.footballscores.model.Goal;
import com.project.footballscores.model.GoalType;
import com.project.footballscores.model.Match;
import com.project.footballscores.model.Matchday;
import com.project.footballscores.model.Season;
import com.project.footballscores.model.Team;
import com.project.footballscores.model.TeamClassificationTableRow;
import com.project.footballscores.repository.CompetitionRepository;

@Service
public class TeamsClassificationService {

    @Autowired
    CompetitionRepository competitionRepository;
    
    public String getClassificationHeader(Long competitionId, Long seasonId, ClassificationType type) {
    	String header = "";
    	
		Competition competition = competitionRepository.findOne(competitionId);
		
		if(competition == null)
			return header;
		
		header += competition.getName();

		Season season = getSeasonById(seasonId, competition);

		if(season == null)
			return header;
    	
    	return header + " " + season.getYear() + "/" + (season.getYear() + 1) + 
    				(type == ClassificationType.AWAY_MATCHES ? " - Away matches" : (type == ClassificationType.HOME_MACTHES ? " - Home matches" : ""));
    }
	
	public List<TeamClassificationTableRow> getTeamsClassification(Long competitionId, Long seasonId, ClassificationType type) {
		Map<String, TeamClassificationTableRow> rows = new HashMap<>();
		
		Competition competition = competitionRepository.findOne(competitionId);
		if(competition == null)
			return new ArrayList<TeamClassificationTableRow>(rows.values());
		
		Season season = getSeasonById(seasonId, competition);

		if(season == null)
			return new ArrayList<TeamClassificationTableRow>(rows.values());

		for(Matchday matchday : season.getMatchdays()) {
			List<Match> matches = matchday.getMatches();
			
			for(Match match : matches) {
				String homeTeam = match.getHomeTeam().getTeam().getName();
				String awayTeam = match.getAwayTeam().getTeam().getName();
				
				if(!rows.containsKey(homeTeam))
					rows.put(homeTeam, new TeamClassificationTableRow(homeTeam));
				
				if(!rows.containsKey(awayTeam))
					rows.put(awayTeam, new TeamClassificationTableRow(awayTeam));
				
				TeamClassificationTableRow homeTeamRow = rows.get(homeTeam);
				TeamClassificationTableRow awayTeamRow = rows.get(awayTeam);
				
				switch(type) {
				case ALL_MATCHES: 
					calculateMatchResultForAwayTeam(match, awayTeamRow);
					calculateMatchResultForHomeTeam(match, homeTeamRow);
					break;
				case AWAY_MATCHES:
					calculateMatchResultForAwayTeam(match, awayTeamRow);
					break;
				case HOME_MACTHES:
					calculateMatchResultForHomeTeam(match, homeTeamRow);
					break;
				}
			}
		}
		
		List<TeamClassificationTableRow> teamsClassification = new ArrayList<TeamClassificationTableRow>(rows.values());
		
		Collections.sort(teamsClassification, new TeamComparator());
		
		for (int i = 1; i <= teamsClassification.size(); i++) {
			teamsClassification.get(i-1).position = i;
		}
		
		return teamsClassification;
	}

	private void calculateMatchResultForHomeTeam(Match match, TeamClassificationTableRow homeTeamRow) {
		int homeTeamGoals = countScoredGoals(match, match.getHomeTeam().getTeam());
		int awayTeamGoals = countScoredGoals(match, match.getAwayTeam().getTeam());
		
		homeTeamRow.scoredGoals += homeTeamGoals;
		homeTeamRow.lostGoals += awayTeamGoals;
		
		if(homeTeamGoals > awayTeamGoals) {
			homeTeamRow.wonMatchesCount++;
			homeTeamRow.points+=3;
		} else if(awayTeamGoals > homeTeamGoals) {
			homeTeamRow.lostMatchesCount++;
		} else {
			homeTeamRow.points++;
			homeTeamRow.drawnMatchesCount++;
		}
	}
	
	private void calculateMatchResultForAwayTeam(Match match, TeamClassificationTableRow awayTeamRow) {
		int homeTeamGoals = countScoredGoals(match, match.getHomeTeam().getTeam());
		int awayTeamGoals = countScoredGoals(match, match.getAwayTeam().getTeam());
		
		awayTeamRow.scoredGoals += awayTeamGoals;
		awayTeamRow.lostGoals += homeTeamGoals;
		
		if(homeTeamGoals > awayTeamGoals) {
			awayTeamRow.lostMatchesCount++;
		} else if(awayTeamGoals > homeTeamGoals) {
			awayTeamRow.wonMatchesCount++;
			awayTeamRow.points+=3;
		} else {
			awayTeamRow.points++;
			awayTeamRow.drawnMatchesCount++;
		}
	}

	private Season getSeasonById(Long seasonId, Competition competition) {
		for (Season season : competition.getSeasons()) {
			if(seasonId == season.getId()){
				return season;
			}
		}
		return null;
	}

	private int countScoredGoals(Match match, Team team) {
		int goalsScored = 0;
		Team secondTeam = match.getAwayTeam().getTeam() == team ? match.getHomeTeam().getTeam() : match.getAwayTeam().getTeam();
		
		for(Goal goal : match.getGoals()) {
			if(goal.getType() != GoalType.OWN_GOAL && team.getPlayers().contains(goal.getPlayer()))
				goalsScored++;
			
			if(goal.getType() == GoalType.OWN_GOAL && secondTeam.getPlayers().contains(goal.getPlayer()))
				goalsScored++;
		}
		
		return goalsScored;
	}

	private class TeamComparator implements Comparator<TeamClassificationTableRow> {
		@Override
		public int compare(TeamClassificationTableRow t1, TeamClassificationTableRow t2) {
			return Integer.compare(t1.points, t2.points) == 0 ? (Integer.compare(t1.scoredGoals, t2.scoredGoals) == 0 ?
					Integer.compare(t1.lostGoals, t2.lostGoals) : -Integer.compare(t1.scoredGoals, t2.scoredGoals)) : 
						-Integer.compare(t1.points, t2.points);
		}
	}
	
	public static enum ClassificationType {
		ALL_MATCHES, HOME_MACTHES, AWAY_MATCHES
	}
}
