package com.project.footballscores.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.footballscores.model.Referee;
import com.project.footballscores.repository.RefereeRepository;

@Service
public class RefereeService {

	@Autowired
	private RefereeRepository refereeRepository;

	public List<Referee> findAllRefereesSortedByMatches() {
		List<Referee> referees = refereeRepository.findAll();
		Collections.sort(referees, new Comparator<Referee>(){

			@Override
			public int compare(Referee o1, Referee o2) {
				return o2.getRefereedMatches().size()-o1.getRefereedMatches().size();
			}
		});
		return referees;
	}

}
