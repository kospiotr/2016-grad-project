package com.project.footballscores.service;

import com.project.footballscores.model.*;
import com.project.footballscores.repository.SeasonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by rjan on 2015-09-04.
 */
@Service
public class CardService {

    @Autowired
    SeasonRepository seasonRepository;

    public List<CardClassificationTableRow> getCardsClassificationForSeason(Long id) {
        Map<String, CardClassificationTableRow> cardClassificationTableRows = new HashMap<>();

        Season season = seasonRepository.findOne(id);
        for (Matchday matchday : season.getMatchdays()) {
            for (Match match : matchday.getMatches()) {
                for (Card card : match.getCards()) {
                    String key = card.getPlayer().getName() + " " + card.getPlayer().getSurname();
                    Short yellowsToAdd = 0;
                    Short redsToAdd = 0;
                    switch (card.getType()) {
                        case YELLOW:
                            yellowsToAdd++;
                            break;
                        case RED:
                            redsToAdd++;
                            break;
                    }
                    if (cardClassificationTableRows.containsKey(key)) {
                        Short yellows = cardClassificationTableRows.get(key).getYellowCardsCount();
                        Short reds = cardClassificationTableRows.get(key).getRedCardsCount();
                        cardClassificationTableRows.get(key).setYellowCardsCount((short) (yellows + yellowsToAdd));
                        cardClassificationTableRows.get(key).setRedCardsCount((short) (reds + redsToAdd));
                    } else {
                        cardClassificationTableRows.put(key, new CardClassificationTableRow(key, yellowsToAdd, redsToAdd));
                    }
                }
            }
        }
        List<CardClassificationTableRow> cards = new ArrayList<CardClassificationTableRow>(cardClassificationTableRows.values());
        Collections.sort(cards, new Comparator<CardClassificationTableRow>() {
            @Override
            public int compare(CardClassificationTableRow o1, CardClassificationTableRow o2) {
                return o2.getYellowCardsCount() - o1.getYellowCardsCount();
            }
        });
        return cards;
    }
}
