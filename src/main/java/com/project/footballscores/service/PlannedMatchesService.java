package com.project.footballscores.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.List;

import com.project.footballscores.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Service;

import com.project.footballscores.repository.CompetitionRepository;
import com.project.footballscores.repository.CompetitionRepository;

/**
 * Created by isiejkowski on 2015-09-04.
 */
@Service
public class PlannedMatchesService {

    public PlannedMatchesService(){}



    @Autowired
    CompetitionRepository competitionRepository;

    public List<PlannedMatchesMatchDayRow> getMatches(Long competitionId, Long seasonId) {

        Long id=new Long(1);


        List<PlannedMatchesMatchDayRow> plannedMatchesMatchDayRowList = new ArrayList<PlannedMatchesMatchDayRow>();


        Competition competition = competitionRepository.findOne(competitionId);

        Season season = getSeasonById(seasonId, competition);

        for(Matchday matchday : season.getMatchdays()) {

            PlannedMatchesMatchDayRow plannedMatchesMatchDayRow =new PlannedMatchesMatchDayRow();

            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            plannedMatchesMatchDayRow.setDate(df.format(matchday.getDate()).toString());
            plannedMatchesMatchDayRow.setId(id);
            id++;

            List<Match> matches = matchday.getMatches();

            for(Match match : matches) {

                PlannedMatchesRow  plannedMatchesRow = new PlannedMatchesRow();

                plannedMatchesRow.setId(match.getId());
                plannedMatchesRow.setAway(match.getAwayTeam().getTeam().getName());
                plannedMatchesRow.setHome(match.getHomeTeam().getTeam().getName());
                plannedMatchesRow.setStadium(match.getStadium().getName());
                plannedMatchesRow.setAwayScore(countScoredGoals(match, match.getAwayTeam().getTeam()));
                plannedMatchesRow.setHomeScore(countScoredGoals(match, match.getHomeTeam().getTeam()));


                plannedMatchesMatchDayRow.add(plannedMatchesRow);

            }

            plannedMatchesMatchDayRowList.add(plannedMatchesMatchDayRow);
        }
        return plannedMatchesMatchDayRowList;

    }

    private Season getSeasonById(Long seasonId, Competition competition) {
        for (Season season : competition.getSeasons()) {
            if(seasonId == season.getId()){
                return season;
            }
        }
        return null;
    }

    private int countScoredGoals(Match match, Team team) {
        int goalsScored = 0;
        Team secondTeam = match.getAwayTeam().getTeam() == team ? match.getHomeTeam().getTeam() : match.getAwayTeam().getTeam();

        for(Goal goal : match.getGoals()) {
            if(goal.getType() != GoalType.OWN_GOAL && team.getPlayers().contains(goal.getPlayer()))
                goalsScored++;

            if(goal.getType() == GoalType.OWN_GOAL && secondTeam.getPlayers().contains(goal.getPlayer()))
                goalsScored++;
        }

        return goalsScored;
    }




}
