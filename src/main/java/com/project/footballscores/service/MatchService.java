package com.project.footballscores.service;

import com.project.footballscores.model.Goal;
import com.project.footballscores.model.GoalType;
import com.project.footballscores.model.Match;
import com.project.footballscores.model.PlannedMatchesRow;
import com.project.footballscores.repository.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;

/**
 * Created by rjan on 2015-09-07.
 */
@Service
public class MatchService {

    @Autowired
    MatchRepository matchRepository;

    public PlannedMatchesRow findById(Long id) {
        Match match = matchRepository.findOne(id);
        PlannedMatchesRow plannedMatchesRow = new PlannedMatchesRow();
        plannedMatchesRow.setId(match.getId());
        plannedMatchesRow.setHome(match.getHomeTeam().getTeam().getName());
        plannedMatchesRow.setAway(match.getAwayTeam().getTeam().getName());
        plannedMatchesRow.setStadium(match.getStadium().getName());
        Integer homeGoals = 0;
        Integer awayGoals = 0;

        for(Goal goal : new LinkedHashSet<>(match.getGoals())) {
            if((goal.getType() != GoalType.OWN_GOAL && match.getHomeTeam().getPlayers().contains(goal.getPlayer())) || (goal.getType() == GoalType.OWN_GOAL && match.getAwayTeam().getPlayers().contains(goal.getPlayer()))) {
                homeGoals++;
            } else {
                awayGoals++;
            }
        }

        plannedMatchesRow.setHomeScore(homeGoals);
        plannedMatchesRow.setAwayScore(awayGoals);

        return plannedMatchesRow;
    }
}
