CREATE SCHEMA example;

USE example;

CREATE TABLE referees (
	id BIGINT NOT NULL AUTO_INCREMENT,
	name VARCHAR(30) NOT NULL,
    surname VARCHAR(30) NOT NULL,
    country VARCHAR(3) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO referees(name, surname, country) VALUES('Adam', 'Adam', 'POL');

CREATE TABLE stadiums (
	id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    capacity INTEGER NOT NULL,
    city VARCHAR(20) NOT NULL,
    country VARCHAR(3) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO stadiums(name, capacity, city, country) VALUES('Emirates', 50000, 'London', 'ENG');

CREATE TABLE competitions (
	id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO competitions(name) VALUES('Premier League');

CREATE TABLE teams (
	id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    stadium_id BIGINT NOT NULL,
	PRIMARY KEY (id),
    FOREIGN KEY (stadium_id)
		REFERENCES stadiums(id)
);

INSERT INTO teams(name, stadium_id) VALUES ('Arsenal London', 1);

CREATE TABLE players (
	id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    surname VARCHAR(30) NOT NULL,
    country VARCHAR(3) NOT NULL,
	position VARCHAR(3) NOT NULL,
	team_id BIGINT NOT NULL,
	PRIMARY KEY (id),
    FOREIGN KEY (team_id)
		REFERENCES teams(id)
);

INSERT INTO players(name, surname, country, position, team_id) VALUES('Example', 'Player', 'POL', 'GK', 1);
INSERT INTO players(name, surname, country, position, team_id) VALUES('Another', 'Player', 'POL', 'CB', 1);

CREATE TABLE coaches (
	id BIGINT NOT NULL AUTO_INCREMENT,
	name VARCHAR(30) NOT NULL,
    surname VARCHAR(30) NOT NULL,
    country VARCHAR(3) NOT NULL,
    team_id BIGINT NOT NULL,
	PRIMARY KEY (id),
    FOREIGN KEY (team_id)
		REFERENCES teams(id)
);

INSERT INTO coaches(name, surname, country, team_id) VALUES('Example', 'Player', 'POL', 1);

CREATE TABLE seasons (
	id BIGINT NOT NULL AUTO_INCREMENT,
    year SMALLINT NOT NULL,
	competition_id BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (competition_id)
		REFERENCES competitions(id)
);

INSERT INTO seasons(year, competition_id) VALUES(2015, 1);

CREATE TABLE matchdays (
	id BIGINT NOT NULL AUTO_INCREMENT,
    date date NOT NULL,
    season_id BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (season_id)
		REFERENCES seasons(id)
);

INSERT INTO matchdays(date, season_id) VALUES(NOW(), 1);

CREATE TABLE seasons_teams (
	id BIGINT NOT NULL AUTO_INCREMENT,
    season_id BIGINT NOT NULL,
    team_id BIGINT NOT NULL,
    PRIMARY KEY (id),
	FOREIGN KEY (season_id)
		REFERENCES seasons(id),
	FOREIGN KEY (team_id)
		REFERENCES teams(id)
);

INSERT INTO seasons_teams(season_id, team_id) VALUES(1,1);

CREATE TABLE matchday_teams (
	id BIGINT NOT NULL AUTO_INCREMENT,
    team_id BIGINT NOT NULL,
    PRIMARY KEY (id),
	FOREIGN KEY (team_id)
		REFERENCES teams(id)
);

INSERT INTO matchday_teams(team_id) VALUES(1);
INSERT INTO matchday_teams(team_id) VALUES(1);

CREATE TABLE matches (
	id BIGINT NOT NULL AUTO_INCREMENT,
    matchday_id BIGINT NOT NULL,
    referee_id BIGINT NOT NULL,
    stadium_id BIGINT NOT NULL,
    matchday_team_home_id BIGINT NOT NULL,
    matchday_team_away_id BIGINT NOT NULL,
    PRIMARY KEY (id),
	FOREIGN KEY (matchday_id)
		REFERENCES matchdays(id),
	FOREIGN KEY (referee_id)
		REFERENCES referees(id),
	FOREIGN KEY (stadium_id)
		REFERENCES stadiums(id),
	FOREIGN KEY (matchday_team_home_id)
		REFERENCES matchday_teams(id),
	FOREIGN KEY (matchday_team_away_id)
		REFERENCES matchday_teams(id)
);

INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(1,1,1,1,2);

CREATE TABLE goals (
	id BIGINT NOT NULL AUTO_INCREMENT,
	minute TINYINT NOT NULL,
    match_id BIGINT NOT NULL,
    player_id BIGINT NOT NULL,
    type VARCHAR(3) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (match_id)
		REFERENCES matches(id),
	FOREIGN KEY (player_id)
		REFERENCES players(id)
);

INSERT INTO goals(minute, match_id, player_id, type) VALUES(57, 1, 1, 'OWN');

CREATE TABLE cards (
	id BIGINT NOT NULL AUTO_INCREMENT,
    minute TINYINT NOT NULL,
    match_id BIGINT NOT NULL,
    player_id BIGINT NOT NULL,
    type VARCHAR(1) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (match_id)
		REFERENCES matches(id),
	FOREIGN KEY (player_id)
		REFERENCES players(id)
);

INSERT INTO cards(minute, match_id, player_id, type) VALUES(14, 1, 1, 'R');

CREATE TABLE changes (
	id BIGINT NOT NULL AUTO_INCREMENT,
	minute TINYINT NOT NULL,
    match_id BIGINT NOT NULL,
    player_in_id BIGINT NOT NULL,
    player_off_id BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (match_id)
		REFERENCES matches(id),
	FOREIGN KEY (player_in_id)
		REFERENCES players(id),
	FOREIGN KEY (player_off_id)
		REFERENCES players(id)
);

INSERT INTO changes(minute, match_id, player_in_id, player_off_id) VALUES(78, 1, 1, 2);

CREATE TABLE matchday_teams_players (
	id BIGINT NOT NULL AUTO_INCREMENT,
    matchday_team_id BIGINT NOT NULL,
    player_id BIGINT NOT NULL,
    PRIMARY KEY (id),
	FOREIGN KEY (matchday_team_id)
		REFERENCES matchday_teams(id),
	FOREIGN KEY (player_id)
		REFERENCES players(id)
);

INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(1, 1);
INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(1, 2);

INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(2, 1);
INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(2, 2);