USE example;


INSERT INTO referees(name, surname, country) VALUES('Stefan', 'Miller', 'GER');
INSERT INTO referees(name, surname, country) VALUES('Fernando', 'Torres', 'ES');


INSERT INTO stadiums(name, capacity, city, country) VALUES('Narodowy', 40000, 'Warszawa', 'POL');
INSERT INTO stadiums(name, capacity, city, country) VALUES('ErgoArena', 40000, 'Gdanks', 'POL');


INSERT INTO competitions(name) VALUES('League1');
INSERT INTO competitions(name) VALUES('Ekstraklasa');


INSERT INTO teams(name, stadium_id) VALUES ('Real Madryt', 2);
INSERT INTO teams(name, stadium_id) VALUES ('Manchester United', 3);
INSERT INTO teams(name, stadium_id) VALUES ('Chelsea London', 3);
INSERT INTO teams(name, stadium_id) VALUES ('West Ham United', 3);
INSERT INTO teams(name, stadium_id) VALUES ('Manchester City', 3);


INSERT INTO players(name, surname, country, position, team_id) VALUES('Another', 'Player', 'POL', 'CB', 1);
INSERT INTO players(name, surname, country, position, team_id) VALUES('Wayne', 'Rooney', 'GB', 'AMF', 3);
INSERT INTO players(name, surname, country, position, team_id) VALUES('Cristiano', 'Ronaldo', 'GB', 'AMF', 4);
INSERT INTO players(name, surname, country, position, team_id) VALUES('', 'Pedro', 'GB', 'AMF', 5);
INSERT INTO players(name, surname, country, position, team_id) VALUES('Diego', 'Costa', 'GB', 'AMF', 5);


INSERT INTO coaches(name, surname, country, team_id) VALUES('Example', 'Player', 'POL', 1);
INSERT INTO coaches(name, surname, country, team_id) VALUES('Example2', 'Surname2', 'POL', 3);


INSERT INTO seasons(year, competition_id) VALUES(2013, 1);
INSERT INTO seasons(year, competition_id) VALUES(2015, 2);


INSERT INTO matchdays(date, season_id) VALUES(NOW(), 2);
INSERT INTO matchdays(date, season_id) VALUES(NOW(), 3);


INSERT INTO seasons_teams(season_id, team_id) VALUES(2,1);
INSERT INTO seasons_teams(season_id, team_id) VALUES(1,2);
INSERT INTO seasons_teams(season_id, team_id) VALUES(1,3);
INSERT INTO seasons_teams(season_id, team_id) VALUES(1,4);
INSERT INTO seasons_teams(season_id, team_id) VALUES(1,5);


INSERT INTO matchday_teams(team_id) VALUES(2);
INSERT INTO matchday_teams(team_id) VALUES(3);
INSERT INTO matchday_teams(team_id) VALUES(4);
INSERT INTO matchday_teams(team_id) VALUES(5);


INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(1,1,1,1,2);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(2,2,3,2,3);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(3,2,3,3,4);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(1,2,3,4,5);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(2,2,3,5,1);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(2,2,3,1,2);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(3,2,3,1,2);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(1,2,3,2,3);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(2,2,3,3,4);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(3,2,3,4,5);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(1,2,3,5,1);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(2,2,3,1,2);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(3,2,3,2,3);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(1,2,3,3,4);
INSERT INTO matches(matchday_id, referee_id, stadium_id, matchday_team_home_id, matchday_team_away_id) VALUES(2,2,3,4,5);


INSERT INTO goals(minute, match_id, player_id, type) VALUES(30, 2, 3, 'PEN');
INSERT INTO goals(minute, match_id, player_id, type) VALUES(40, 3, 2, 'NOR');
INSERT INTO goals(minute, match_id, player_id, type) VALUES(40, 4, 2, 'NOR');
INSERT INTO goals(minute, match_id, player_id, type) VALUES(40, 5, 3, 'NOR');
INSERT INTO goals(minute, match_id, player_id, type) VALUES(40, 6, 3, 'NOR');
INSERT INTO goals(minute, match_id, player_id, type) VALUES(40, 7, 2, 'NOR');
INSERT INTO goals(minute, match_id, player_id, type) VALUES(40, 8, 2, 'NOR');
INSERT INTO goals(minute, match_id, player_id, type) VALUES(40, 6, 3, 'NOR');
INSERT INTO goals(minute, match_id, player_id, type) VALUES(40, 6, 3, 'NOR');
INSERT INTO goals(minute, match_id, player_id, type) VALUES(40, 7, 4, 'NOR');
INSERT INTO goals(minute, match_id, player_id, type) VALUES(40, 4, 4, 'NOR');
INSERT INTO goals(minute, match_id, player_id, type) VALUES(40, 4, 1, 'NOR');
INSERT INTO goals(minute, match_id, player_id, type) VALUES(40, 3, 2, 'NOR');


INSERT INTO cards(minute, match_id, player_id, type) VALUES(89, 2, 3, 'Y');
INSERT INTO cards(minute, match_id, player_id, type) VALUES(88, 3, 2, 'Y');


INSERT INTO changes(minute, match_id, player_in_id, player_off_id) VALUES(88, 2, 3, 2);
INSERT INTO changes(minute, match_id, player_in_id, player_off_id) VALUES(94, 3, 1, 3);


INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(1, 1);
INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(1, 2);
INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(3, 2);
INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(4, 2);
INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(5, 2);
INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(1, 2);
INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(1, 2);
INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(1, 2);
INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(1, 2);

INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(2, 1);
INSERT INTO matchday_teams_players(matchday_team_id, player_id) VALUES(2, 2);
